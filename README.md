# P2: Multi-Scale SZ Detection


 ______________________________________________________________________________________  
/	This project aims to analyse and separate different astrophysical components in    \  
\	multi-frequency	sky maps using the ILC method with some improvements.              /  
 --------------------------------------------------------------------------------------  
				  \
				   \

				    A__A
				   ( OO )\_----__
				   (____)\ MILCA )\/\
					||           |
					||`------w--||

## Getting Started

The project covers the implementation of ILC algorithm under constrains for 3 different sources (CMB, SZ and AGN), with some additionnal functionalities.
For example one can use a mask of the galaxy, and a multiscale analysis using gate functions. 

This project also contains some scripts in order to preprocess the maps first.
A first implementation uses lightened data (put in data/) containing the maps for each frequency with a degraded pixellisation (NSIDE = 512).
The prepare_map script takes a map as argument, the pixellisation wanted, the beam reduction given by the worst frequency associated (100GHz in our case), and gives b ack the prepared map.
This takes into account the units conversion for 857 and ~585~ GHz maps.

The scripts implemented contain programs for running ILC algorithm (using Lagrange multipliers), with and without constrains, coded in compute_sources_*.py 

## Running the tests

The main program can be run by calling **god_program.py**. This script asks for manny parameters such as the pixellisation (512 or 2048), using a mask of the galaxy or not, ILC constrain with or without radio source, and using multiscaling analysis or not (put 0 or 1 for each parameter)
Then one can launch some other scripts to get the spectrum and noise.

## References

* Planck 2015 VIII: HFI data processing: Calibration and maps  arXiv:astro-ph/1502.01587
* Planck 2013 VII. HFI time responses and beam arXiv:astro-ph/1303.5068
* Planck 2013 IX.  HFI spectral response arXiv:astro-ph/1303.5070

## Authors

* **Alexandre Toubiana, Louise Mousset, Brian Ventura**


## License

MILCA is a brand of chocolate confection which originated in Switzerland in 1901 and has been manufactured internationally by the US confectionery company Mondelēz International (formerly known as Kraft Foods) since 1990.




\documentclass{article}

%français, accents...
\usepackage[french]{babel}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}

\usepackage{geometry} %pour les marges
\usepackage{amsmath} %paquet de maths
\usepackage{graphicx}%pour inclure des images
\usepackage{subfigure}%pour mettre des figures côte à côte
\usepackage{caption}%pour les légendes des figures
\usepackage{comment} %pour des environnements commentaires

\geometry{hmargin=2.5cm,vmargin=2.5cm}

\title{Détection des amas de galaxies dans la globalité du ciel à l'aide de l'effet Sunyaev-Zeldovich}
\author{Alexandre Toubiana, Brian Ventura, Louise Mousset}
\date{}

\begin{document}
\maketitle

\section*{Introduction}
Le travail présenté a été réalisé à partir des données du satellite Planck. Le but était de reconstruire la carte  de l'émission Sunyaev-Zeldovich (SZ) afin d'étudier les amas de galaxies. En effet, en traversant ces amas, les photons du CMB intéragissent avec les électrons par effet Compton inverse. Cela modifie leur énergie et induit alors une distorsion du spectre tout à fait caractéristique. L'observation d'émission SZ peut ainsi être reliée à la présence d'un amas de galaxies. 
Dans ce projet, le ciel est considéré dans sa totalité.\\
\\
%annonce du plan
Tout d'abord, l'algorithme utilisé sera décrit d'un point de vue théorique. Puis nous présenterons la démarche adoptée et les résultats obtenus. Enfin, la cohérence de ces résultats sera évaluée avec un point de vue critique et nous reviendrons sur certaines imperfections et possibles améliorations.


%============================
\section{L'algorithme ILC}
Cet algorithme est décrit dans \cite{hurier2013milca}.
Dans un premier temps, formalisons les données des cartes sur lesquelles cet algorithme d'extraction sera appliqué. Nous avons travaillé sur un ensemble de 6 cartes (indexées par un indice $i$) correspondant chacune à une observation du ciel sur une bande de fréquences particulière. Le signal détecté par Planck est une superposition de signaux d'origines physiques diverses : CMB "pur", CMB distordu par effet SZ, radiation infrarouge de la poussière (CIB)... Les $N_s$ sources présentes dans une cartes seront indexées par l'indice $j$. Nous pouvons ainsi écrire $$T_{i}(p) = \sum_{j=1}^{N_s}A_{ij}(p)S_j(p)+n_j(p)$$ où les $S_j(p)$ sont les cartes des différentes sources, $n_j$ le bruit de l'instrument, et les $A_{ij}$ regroupant la dépendance fréquentielle des différentes sources (indice $j$) convoluée avec la réponse spectrale du détecteur propre à chaque fréquence d'observation (indice $i$).
En supposant que la matrice $A_{ij}$ précédente est constante sur tout le ciel, l'équation précédente s'écrira matriciellement $$ T(p)= AS(p)+N(p)$$ 
Afin d'extraire les différentes sources composant le vecteur $S$, nous avons utilisé l'algorithme appelé "Internal Linear Combination", qui permet de considérer les reconstructions $\widehat{S_j}(p)$ comme combinaisons linéaires des cartes de base $T_i(p)$ avec des poids $\omega_i$, i.e. $\widehat{S_j}(p) = \omega^TT(p)$.
L'enjeu du problème étant de trouver ces poids, l'algorithme ILC est en fait un problème d'optimisation sous contraintes basé sur le théorème des multiplicateurs de Lagrange. La fonction de coût à minimiser est la variance de $\widehat{S_j}(p)$, que l'on peut aussi écrire sous la forme $$Var(\widehat{S_j}(p))=\omega^T<T'T'^T>\omega$$ où $<T'T'^T> = C_T$ où $T'=T-<T>$ représente la matrice de covariance associée aux cartes et moyennée sur les pixels. Il y a maintenant différentes contraintes possibles à appliquer à cette fonction de coût.

\subsection{Contraintes simples}
Rappelons que la matrice $A_{ij}$ est composée sur ses lignes des différentes fréquences observées, et sur ses colonnes des différentes sources. Cela étant, si l'on veut extraire une source spécifique $j_0$, on peut fixer une contrainte de la forme $$g_{j_0}=\omega^Tf_{j_0}=1$$ où $f_{j_0}$ est la $j_0^{eme}$ colonne de $A_{ij}$. Par cette unique contrainte, on arrive à extraire, par le théorème des multiplicateurs de Lagrange, des poids $\omega_i$ permettant de reconstruire $\widehat{S_j}(p) = \omega^TT(p)$, grâce à la formule $$\widehat{S_{j_0}}(p) = (f_{j_0}^TC_T^{-1}f_{j_0})^{-1}f_{j_0}^TC_TT(p)$$

\subsection{Contraintes croisées}
Le résultat précédent permet d'extraire des cartes initiales, certaines sources astrophysiques. Il est possible dans certains cas d'avoir un résultat plus précis en imposant comme contrainte supplémentaire de rejeter les autres sources astrophysiques lors de l'extraction de l'une d'entre elles. Il faut néanmoins pour cela connaître les spectres d'émission d'autres sources. Ainsi au lieu de simplement contraindre par $g=\omega^Tf_{j_0}=1$, on peut donc ajouter $$g_{j \ne j_0}=\omega^Tf_{j \ne j_0}=0$$ pour chaque source $j \ne j_0$ devant être rejetée. On peut ainsi regrouper matriciellement ces contraintes, extraire les poids $\omega_i$ associés, et de la même façon que précédemment, reconstruire  $\widehat{S_j}(p) = \omega^TT(p)$


Une question intéressante peut alors se poser lorsque l'on utilise ces contraintes croisées, en regardant les formules d'extraction de $\widehat{S_{j_0}}(p)$. En effet, la formule d'extraction devient $$\widehat{S_{j_0}}(p) = \{e_{j_0}^T(F^TC_T^{-1}F)^{-1}F^TC_T^{-1}\}T(p)$$

En accord avec la convention de \cite{hurier2013milca} nous avons normalisé le spectre des sources par rapport au spectre du CMB. Ainsi les données utilisées pour le spectre du SZ sont celles de \cite{hurier2013milca} et pour le spectre de l'émission radio nous avons utilisé une loi de puissance d'indice -0.7 en accord avec \cite{aghanim2015good}. Il est à noter que la normalisation du spectre d'émission radio n'intervient pas dans le résultat obtenu pour les cartes CMB et SZ, cela peut se montrer analytiquement à l'aide de matrices de dilatation appliquées à la matrice $F$ regroupant les différentes dépendances spectrales (lignes) des différentes sources (colonnes).

%Si l'on considère la matrice $F$, les dépendances spectrales des différentes sources sontsur les différentes colonnes, et les différentes fréquences sur les lignes.

%%Ainsi, multiplier une source par un facteur multiplicatif quelconque revient à multiplier une colonne de la matrice $F$ par ce facteur. Cela se formalise à l'aide de matrice de dilatation $D_{i_0}$. Multiplier F par $D_{i_0}$ à gauche revient à multiplier la ligne $i_0$ de F par $\alpha$ et multiplier à droite par $D_{i_0}$ revient à multiplier la colonne $i_0$.
%%Ainsi notre problème revient à voir si utiliser la matrice $F$ ou la matrice $F'=FD_{i_0}$ change l'extraction de $\widehat{S_{SZ=j_0}}(p)$.
%%En remplaçant donc $F$ par $F'$ dans la formule, on obtient
%%$$\widehat{S_{j_0}}(p) = \{e_{j_0}^T(F'^TC_T^{-1}F')^{-1}F'^TC_T^{-1}\}T(p)$$
%%$$\widehat{S_{j_0}}(p) = \{e_{j_0}^T(D_{i_0}^TF^TC_T^{-1}FD_{i_0})^{-1}D_{i_0}^TF^TC_T^{-1}\}T(p)$$
%%$$\widehat{S_{j_0}}(p) = \{e_{j_0}^T(D_{i_0}^{-1}F^{-1}C_TF^{T^{-1}}D_{i_0}^{T^{-1}})D_{i_0}^TF^TC_T^{-1}\}T(p)$$
%%$$\widehat{S_{j_0}}(p) = \{e_{j_0}^T(D_{i_0}^{-1}F^{-1}C_TC_T^{-1}\}T(p)$$

%============================
\section{Présentation progressive des résultats}
Au départ extrêmement simple, l'analyse a été peu à peu complexifiée afin de prendre en compte un nombre grandissant d'effets et d'affiner les résultats obtenus. La démarche adoptée est présentée pas à pas.

\subsection{Préparation des cartes}
La combinaison linéaire de différentes cartes doit se faire avec des cartes de même résolution. Pour cela, on s'aligne sur la pire résolution qui est 10 arcmin en convoluant par une gaussienne dont la variance $\sigma$ se calcule par: $\sigma^2_{carte}+\sigma^2 = \sigma^2_{10'}$.

Par ailleurs, la température mesurée en chaque point du ciel est exprimée en unité de $K_{CMB}$, ce qui revient à normaliser les spectres des différentes sources par rapport à celui du CMB. Pour travailler dans cette unité, il faut alors appliquer les facteurs de conversions adaptés \cite{ade2014planck}.

\subsection{Masquage de la galaxie}
Sur les cartes réalisées par Planck, l'émission de notre galaxie est très largement dominante. Afin de débarasser les données de cette émission prépondérante, un masque couvrant la Voie Lactée a été réalisé. Nous l'avons construit à partir de la carte observée autour d'une fréquence moyenne de 857 GHz pour laquelle l'émission de poussière de la galaxie est la plus prononcée. Les pixels dépassant un seuil fixé de telle sorte à masquer environ 15$\%$ du ciel (comme indiqué dans \cite{aghanim2015good}) ont été supprimés.\\

La Figure \ref{mask} présente le masque obtenu qui est ensuite appliqué à chaque carte utilisée par l'algorithme ILC. 

%image du mask et carte 857
\begin{figure}[!h]
  \centering
  \subfigure[carte à 857 GHz]{\includegraphics[scale = 0.3]{plots/Planck_857.png}}
  \hspace{2cm}
  \subfigure[masque]{\includegraphics[scale = 0.3]{plots/mask_014.png}}
\label{mask}
\caption{Carte de Planck et masque obtenu à partir de cette carte}
\end{figure}

Afin de montrer la pertinence de l'ajout d'un masque, nous avons tracé en Figure \ref{spectre} les spectres de puissance du CMB avec et sans masque.

\begin{figure}[!h]
  \centering
  \subfigure[sans masque]{\includegraphics[scale = 0.4]{plots/spectre_CMB_sans_mask.png}}
  \hspace{2cm}
  \subfigure[avec masque]{\includegraphics[scale = 0.4]{plots/spectre_CMB_mask.png}}
\label{spectre}
\caption{Spectre du CMB obtenu avec et sans masquage de la galaxie}
\end{figure}

\subsection{Prise en compte des sources radio}
Comme expliqué précédemment, l'algorithme ILC contraint permet de considérer un nombre quelconque de sources à condition de connaître leurs caractéristiques physiques. Nous avons d'abord considéré uniquement 2 sources, le CMB "pur" et le SZ dont les caractéristiques sont données dans l'article \cite{ade2014planck}. Cette première analyse a permis d'effectuer un premier relevé des amas mais un défaut a été noté. Comme le montre la Figure \ref{persee} l'amas de Persée semble présenter un trou au centre de son émission. Néanmoins, une émission ponctuelle au centre de l'amas est visible, et cela pourrait s'expliquer par un défaut d'implémentation de l'ILC qui n'a pu être résolu. 

Les noyaux actifs de galaxies (AGN) présents dans certains amas comme celui de Persée sont connus pour émettre des fréquences radio dont le spectre est bien connu \cite{aghanim2015good}. Cette troisième source a alors été prise en compte et le résultat est visible sur la Figure \ref{persee}. Cependant, suite au défaut mentionné ci-dessus, l'amélioration n'est pas concluante.

\begin{figure}[!h]
  \centering
  \subfigure[sans masque]{\includegraphics[scale = 0.3]{plots/SZ_2048_mask1_radio0_cluster_Perseus.png}}
  \hspace{2cm}
  \subfigure[avec masque]{\includegraphics[scale = 0.3]{plots/SZ_2048_mask1_radio1_cluster_Perseus.png}}
\label{persee}
\caption{Amas Perseus avec et sans traitement radio}
\end{figure}


\subsection{Analyse multi-échelles}

Jusqu'à présent nous n'avons pas pleinement pris en compte la dépendance en échelle des spectres d'émission. En séparant la contribution des différentes échelles à la carte initiale et en appliquant l'algorithme ILC à chacune des cartes obtenues il est possible de rendre compte de cette dépendance. 

L'image du ciel étant une sphère, on peut passer dans l'espace des fréquences spatiales à l'aide de la décomposition en harmoniques sphériques:

\begin{equation}
T = \sum_{l,m} a_{l,m} Y_l^m(\theta, \phi)
\end{equation}
où T est le signal que l'on souhaite décomposer.

On définit $C_l = <a^*_{l,m}a_{l,m}>$ où la moyenne doit se faire sur un grand nombre d'univers. Comme cela est impossible on fait plutôt une moyenne sur m mais cela est équivalent. Le spectre de puissance s'obtient alors en traçant $D_l = \frac{l(l+1)}{2\pi} C_l$ en fonction de $l$. Ainsi, on a $l\sim \frac{\pi}{\theta}$ et on peut couper le spectre pour séparer les différentes échelles spatiales.

Pour commencer, nous avons séparé le spectre en 3 zones à l'aide d'une fonction porte. Le résultat obtenu est présenté sur la Figure \ref{multiscale}.

%image 2048_SZ avec et sans mask radio1
\begin{figure}[!h]
  \centering
  \subfigure[sans masque]{\includegraphics[scale = 0.3]{plots/SZ_2048_mask0_radio1_multiscale.png}}
  \hspace{2cm}
  \subfigure[avec masque]{\includegraphics[scale = 0.3]{plots/SZ_2048_mask1_radio1_multiscale.png}}
\label{multiscale}
\caption{Carte SZ avec et sans traitement multi-échelles}
\end{figure}

Cette coupure brutale a l'inconvénient de créer du ringing dans la carte SZ reconstruite. Cela est dû au fait que la transformée d'une porte est un sinus cardinal qui apparait sur la figure \ref{multiscale}. 




%============================
\section{Cohérence et validation des résultats obtenus}
Afin de vérfier la pertinence des résultats obtenus, il est important de les comparer à des résultats déjà connus. Pour ce faire, nous cherchons à reproduire le spectre du CMB, une des mesures cosmologiques existantes la plus précise. Par ailleurs, nous vérifions que nous observons bien une émission de SZ là où des amas ont déjà été catalogués. Finalement le spectre du SZ obtenu est comparé avec celui publié dans l'analyse des données de Planck \cite{ade2014planck}. Nous présentons tout d'abord comment a été traité le bruit dans la reconstruction des spectres. 

\subsection{Séparation du bruit}
La prise de données par Planck a été faite en séparant les cartes obtenues pour chaque fréquence en deux parties correspondant à des instant successifs séparés d'un cours laps de temps. Ainsi, en admettant que l'univers observé est le même pour ces deux ensembles de cartes, la seule différence entre elles est le bruit induit par la carte de lecture. Ainsi, en appliquant l'algorithme ILC aux cartes totales afin d'obtenir les poids puis en sommant les cartes de chacun des ensembles avec ces poids, nous obtenons deux cartes pour chacune des sources (SZ ou CMB dans notre cas) et on peut écrire:
\begin{equation}
T_i=G*S+N_i \ \ \ \ i=1,2
\end{equation}
Où $S$ est la carte originale, $G$ est la gaussienne de FWHM 10 arcmin représentant l'effet du miroir sur l'image originale, $N_i$ est le bruit et $T_i$ est la carte obtenue suite au traitement des cartes de l'ensemble 1 ou 2.
En faisant une décomposition en harmoniques sphériques, nous obtenons :

\begin{equation}\label{sph_trans}
\hat{T}_i=\hat{G} \hat{S}+\hat{N}_i \ \ \ \ i=1,2
\end{equation}

Ansi, $\hat{T}_1-\hat{T}_2=\hat{N}_1-\hat{N}_2$ donc en prenant le spectre de puissance de  $\frac{\hat{T}_1-\hat{T}_2}{2}$, nous obtenons le spectre de puissance du bruit (puisque celui-ci est le même pour toute réalisation du bruit). Pour un bruit blanc, le spectre devrait être quasiment plat, ce qui n'est pas tout à fait vérifié comme on peut le voir sur la Figure \ref{bruit}.

\begin{figure}[!h]
  \centering
  \includegraphics[scale = 0.5]{plots/spectrum_noise_CMB.png}
\label{bruit}
\caption{Spectre du bruit}
\end{figure}

Une fois ce calcul fait, nous pouvons obtenir le spectre de puissance de la source étudiée en calculant le spectre de puissance de la carte totale. En faisant l'hypothèse que le signal et le bruit sont décorrélés et que celui-ci a une moyenne nulle on obtient:

\begin{equation*}
C_{l,obs}=G_l^2C_{l,source}+C_{l,bruit}
\end{equation*}
Où $G_l$ est la transformée de la gaussienne.
Et donc:

\begin{equation}
C_{l,source}=\frac{C_{l,obs}-C_{l,bruit}}{G_l^2}
\end{equation}

Cette méthode permet de réduire la contribution du bruit au spectre de puissance.

\subsection{Spectre du CMB}

Nous avons appliqué la méthode décrite précédemment à nos cartes afin d'obtenir le spectre du CMB. La figure \ref{spectre_bruit} présente les résultats obtenus en calculant le spectre du CMB avec et sans le traitement du bruit décrit ci-dessus à partir des cartes obtenues en utilisant l'ILC contraint avec radio sur des cartes masquées. 

\begin{figure}[!h]
  \centering
  \subfigure[sans traitement du bruit]{\includegraphics[scale = 0.3]{plots/spectrum_CMB_no_noise_treatment.png}}
  \hspace{2cm}
  \subfigure[avec traitement du bruit]{\includegraphics[scale = 0.3]{plots/spectrum_CMB_noise_treatment.png}}
\label{spectre_bruit}
\caption{Spectre du CMB avec et sans le traitement du bruit }
\end{figure}

La principale amélioration apportée par le traitement du bruit est le réhaussement des pics impairs par rapport aux pics pairs, ce qui est observé sur le spectre du CMB divulgué par Planck. 
Néanmoins, nous observons une explosion du spectre aux petites échelles qui signale la présence d'un problème non identifié. 

\subsection{Présence des amas et spectre du SZ}
Outre Persée que nous avons évoqué précédemment, d'autres amas catalogués peuvent être identifiés sur la carte SZ que nous avons obtenue. Dans la figure \ref{amas} on peut voir Perseus, Virgo, A2199 et A3627.

\begin{figure}[!h]
  \centering
  \subfigure[Perseus]{\includegraphics[scale = 0.3]{plots/SZ_2048_mask1_radio1_multiscale_cluster_Perseus.png}}
  \hspace{2cm}
  \subfigure[Virgo]{\includegraphics[scale = 0.3]{plots/SZ_2048_mask1_radio1_multiscale_cluster_Virgo.png}}
  \hspace{2cm}
  \subfigure[A2199]{\includegraphics[scale = 0.3]{plots/SZ_2048_mask1_radio1_multiscale_cluster_A2199.png}}
  \hspace{2cm}
  \subfigure[A3627]{\includegraphics[scale = 0.3]{plots/SZ_2048_mask1_radio1_multiscale_cluster_A3627.png}}
\label{amas}
\caption{Amas détectés dans la carte SZ}
\end{figure}

La carte SZ étant cohérente avec les résultats déjà connus, nous avons cherché à obtenir le spectre de puissance du SZ présenté en figure \ref{spectreSZ}. Nous appliquons ainsi la même méthode de traitement du bruit que précédemment.

\begin{figure}[!h]
  \centering
  \includegraphics[scale = 0.3]{plots/spectrum_SZ_noise_treatment.png}
\label{spectreSZ}
\caption{Spectre du SZ obtenu après traitement du bruit}
\end{figure}


Malgré d'importantes différences aux grandes échelles, le résultat aux petites échelles est comparable à celui présenté dans \cite{ade2013planck}.  

\newpage
\section*{Conclusion et perspectives}

Ainsi, en partant de la version initiale de l'ILC et en le complexifiant progressivement tout en améliorant le traitement des cartes initiales il a été possible d'obtenir des résultats satisfaisants. Nous avons notamment pu obtenir la carte et le spectre de puissance du SZ, riches en informations pour l'étude des amas de galaxies. Néanmoins, la méthode employée dans ce travail présente un certain nombre de défauts et pourrait être encore améliorée. Un problème dans l'implémentation de l'ILC basique n'a pu être détecté dans le temps imparti.

Au delà de ce problème, une première amélioration qui pourrait être apportée est de lisser le masque qui a été appliqué à la carte initiale pour éviter des passages à 0 trop brutaux. Dans le même ordre d'idée, il serait intéressant d'appliquer des filtres autres que des portes lors de l'analyse multi-échelles \cite{aghanim2016planck}, notamment en imposant un certain chevauchement entre les filtres. Ces modifications permettraient de réduire le ringing et d'obtenir de meilleurs résultats.

Finalement, l'intégralité des résultats présentés dans ce travail a été obtenue en utilisant des variations de l'algorithme ILC dont le principal atout est sa simplicité. Néanmoins, celui-ci ne prend pas en compte une possible corrélation entre les différentes sources et ne traite pas le bruit de manière adéquate. Ces effets sont considérés dans l'algorithme plus avancé Modified Internal Linear Combination Algorithm (MILCA). Son utilisation s'est déjà avérée avantageuse \cite{hurier2013milca} et permettrait de faire une étude plus poussée de l'univers.

%pour les references, j'ai créé un .bib
\bibliography{NPAC_projet_IAS}{}
\bibliographystyle{unsrt}
\nocite{*}

\end{document}

#Script to make the spherical harmonic spectrum of a map

import numpy as np
import healpy as hp
import matplotlib.pyplot as plt

def spectrum(fits):
    map = hp.read_map(fits)
    power_spec = hp.sphtfunc.anafast(map)
    l = np.arange(0, len(power_spec))
    Dl = l*(l+1)/(2*np.pi)*power_spec
    plt.semilogx(l,Dl)
    plt.show()
    return l, Dl

#ILC constrained algorithme with 3 sources: CMB, SZ and radio

import healpy as hp
import numpy as np
import prepare_map
import matplotlib.pyplot as plt
import compute_sources_ILC_constrained_radio

freq_list = ["100","143","217","353","545","857"]

#Create a list containing all the maps prepared
mod_maps = []
i=0
for freq in freq_list:
	mod_maps.append(hp.read_map("/home/abeelen/Planck/maps/HFI_SkyMap_" + freq + "_2048_R2.00_full.fits"))
	mod_maps[i] = prepare_map.prepare_map(mod_maps[i], 2048, freq, 100)
	i += 1

#compute the 3 different sources
CMB_radio ,SZ_radio, RADIO = compute_sources_ILC_constrained_radio.algo_ILC_constrained_radio(mod_maps)

#Show images of maps
plt.ion()

hp.mollview(CMB_radio,min=-5.E-5,max=5.E-5)
hp.mollview(SZ_radio,min=-5.E-5,max=5.E-5)
hp.mollview(RADIO,min=-5.E-5,max=5.E-5)

#Script which open a .fits

import numpy as np
import healpy as hp
import matplotlib.pyplot as plt

def open(fits, mini=-5e-5, maxi=5e-5):
    map = hp.read_map(fits)
    hp.mollview(map, min=mini, max=maxi)
	
    return map
plt.ion()
    

#This script prepare all the original maps, puting them in NSIDE = 512 and write the new map in the data folder

import numpy as np
import healpy as hp
import prepare_map as pmap
import galaxy_mask

#List containing all frequences for each map 
freq_list = ["100","143","217","353","545","857"]

#We read and prepare the maps:
#decrease the pixel number to nside=512
#put all maps at the same resolution
#write them in the data folder

map_857=hp.read_map("/home/abeelen/Planck/maps/HFI_SkyMap_857_2048_R2.00_full.fits")
the_mask = galaxy_mask.mask(map_857,15.)

for freq in freq_list:
        the_map1 = hp.read_map("/home/abeelen/Planck/maps/HFI_SkyMap_" + freq + "_2048_R2.00_halfmission-1.fits")
        pmap.prepare_map(the_map1, 2048, freq, 100, 0, "/home/atoubian/HFI_SkyMap_"+freq+"_2048_homogenized_masked_R2.00_half1.fits")
        the_map1 = np.ma.masked_array(the_map1, mask=the_mask, fill_value=0)
        hp.write_map("/home/atoubian/HFI_SkyMap_"+freq+"_2048_homogenized_masked_R2.00_half1.fits",the_map1)
        the_map2 = hp.read_map("/home/abeelen/Planck/maps/HFI_SkyMap_" + freq + "_2048_R2.00_halfmission-2.fits")
        the_map2=pmap.prepare_map(the_map2, 2048, freq, 100, 0, "/home/atoubian/HFI_SkyMap_"+freq+"_2048_homogenized_masked_R2.00_half2.fits")
        the_map2 = np.ma.masked_array(the_map2, mask=the_mask, fill_value=0)
        hp.write_map("/home/atoubian/HFI_SkyMap_"+freq+"_2048_homogenized_masked_R2.00_half2.fits",the_map2)


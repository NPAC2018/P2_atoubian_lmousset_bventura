#Make maps with a mask on the Milky Way

import numpy as np
import matplotlib.pyplot as plt
import healpy as hp


def mask(map,threshold):
    """create a copy of the map where the pixels above the threshold are 0 and the others 1"""
    the_mask = np.zeros(len(map))
    the_mask[map>threshold] = 1
    return the_mask

def show_mask():
	#Open the map
	map = hp.read_map("/home/abeelen/Planck/maps/HFI_SkyMap_857_2048_R2.00_full.fits")
	n_pixels = len(map)

	plt.ion()
	hp.mollview(map, norm='hist')

	#Create and show the mask
	mask = mask(map,6.3)
	hp.mollview(mask, norm='hist')

	#compute the ratio of pixels masked, it should be ~0.15
	ratio = mask.sum()/n_pixels
	print (ratio)

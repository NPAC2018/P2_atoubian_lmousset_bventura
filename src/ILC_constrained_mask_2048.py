#ILC constrained algorithme adding a mask on the galaxy

import healpy as hp
import numpy as np
import prepare_map
import matplotlib.pyplot as plt
import compute_sources_ILC_constrained
import galaxy_mask

freq_list = ["100","143","217","353","545","857"]

#Create a list containing all the maps, prepare all of them
a=np.ma.zeros(50331648)
mod_maps = np.ma.array([a,a,a,a,a,a])
i=0
for freq in freq_list:
	mod_maps[i] = hp.read_map("/home/abeelen/Planck/maps/HFI_SkyMap_" + freq + "_2048_R2.00_full.fits")
	mod_maps[i] = prepare_map.prepare_map(mod_maps[i], 2048, freq, 100)
	#create the mask with the map at 857
	if (freq =="857"):
		the_mask = galaxy_mask.mask(mod_maps[i],6.3)
	i += 1


#Mask each map with the mask compute before
for j in range (0, len(freq_list)):
	mod_maps[j] = np.ma.masked_array(mod_maps[j], mask=the_mask)

W_mat_mask_2048, CMB_mask_2048, SZ_mask_2048 =compute_sources_ILC_constrained.algo_ILC_constrained(mod_maps)


plt.ion()
hp.mollview(map_100,min=-5.E-5,max=5.e-5)
hp.mollview(CMB_mask_2048,min=-5.E-5,max=5e-5)
hp.mollview(SZ_mask_2048,min=-5.E-5,max=5e-5)

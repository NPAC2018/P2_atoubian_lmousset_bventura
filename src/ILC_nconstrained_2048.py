#premier test with ILC algorithme

import healpy as hp
import numpy as np
import prepare_map
import matplotlib.pyplot as plt
import compute_sources_ILC
import matplotlib.pyplot as plt


map_100 = hp.read_map("/home/abeelen/Planck/maps/HFI_SkyMap_100_2048_R2.00_full.fits")
map_143 = hp.read_map("/home/abeelen/Planck/maps/HFI_SkyMap_143_2048_R2.00_full.fits")
map_217 = hp.read_map("/home/abeelen/Planck/maps/HFI_SkyMap_217_2048_R2.00_full.fits")
map_353 = hp.read_map("/home/abeelen/Planck/maps/HFI_SkyMap_353_2048_R2.00_full.fits")
map_545 = hp.read_map("/home/abeelen/Planck/maps/HFI_SkyMap_545_2048_R2.00_full.fits")
map_857 = hp.read_map("/home/abeelen/Planck/maps/HFI_SkyMap_857_2048_R2.00_full.fits")


#We prepare the maps:
#decrease the pixel number to nside=512
#put all maps at the same resolution
map_100 = prepare_map.prepare_map(map_100, 2048, 100, 100)
map_143 = prepare_map.prepare_map(map_143, 2048, 143, 100)
map_217 = prepare_map.prepare_map(map_217, 2048, 217, 100)
map_353 = prepare_map.prepare_map(map_353, 2048, 353, 100)
map_545 = prepare_map.prepare_map(map_545, 2048, 545, 100)
map_857 = prepare_map.prepare_map(map_857, 2048, 857, 100)

mod_maps = [map_100,map_143,map_217,map_353,map_545,map_857]
CMB=compute_sources_ILC.algo_ILC(mod_maps)[0]
SZ=compute_sources_ILC.algo_ILC(mod_maps)[1]


plt.ion()

hp.mollview(CMB,min=-5.E-5,max=5.E-5)
hp.mollview(SZ,min=-5.E-5,max=5.E-5)

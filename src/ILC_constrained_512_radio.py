#premier test with ILC constrained algorithme

import healpy as hp
import numpy as np
import prepare_map
import matplotlib.pyplot as plt
import compute_sources_ILC_constrained_radio

freq_list = ["100","143","217","353","545","857"]

#Create a list containing all the maps
mod_maps = []
for freq in freq_list:
	mod_maps.append(hp.read_map("../data/HFI_SkyMap_" + freq +"_512_homogenized_R2.00_full.fits"))


CMB_radio, SZ_radio, RADIO = compute_sources_ILC_constrained_radio.algo_ILC_constrained_radio(mod_maps)

plt.ion()

hp.mollview(CMB_radio,min=-5.E-5,max=5.E-5)
hp.mollview(SZ_radio,min=-5.E-5,max=5.E-5)
hp.mollview(RADIO,min=-5.E-5,max=5.E-5)

#premier test with ILC constrained algorithme

import healpy as hp
import numpy as np
import prepare_map
import matplotlib.pyplot as plt
import compute_sources_ILC_constrained

freq_list = ["100","143","217","353","545","857"]

#Create a list containing all the maps
mod_maps = []
i=0
for freq in freq_list:
	mod_maps.append(hp.read_map("/home/abeelen/Planck/maps/HFI_SkyMap_" + freq + "_2048_R2.00_full.fits"))
	mod_maps[i] = prepare_map.prepare_map(mod_maps[i], 2048, freq, 100)
	i += 1


W_mat, CMB, SZ = compute_sources_ILC_constrained.algo_ILC_constrained(mod_maps)


plt.ion()

hp.mollview(CMB,min=-5.E-5,max=5.E-5)
hp.mollview(SZ,min=-5.E-5,max=5.E-5)


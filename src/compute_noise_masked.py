#compute the noise

import healpy as hp
import numpy as np
import prepare_map
import matplotlib.pyplot as plt
import compute_sources_ILC_constrained
import galaxy_mask

freq_list = ["100","143","217","353","545","857"]

#Create a list containing all the maps, prepare all of them
a=np.ma.zeros(12*512**2)
mod_maps = np.ma.array([a,a,a,a,a,a])
i=0
for freq in freq_list:
	mod_maps[i] = hp.read_map("/home/abeelen/Planck/maps/HFI_SkyMap_" + freq + "_2048_R2.00_full.fits")
	mod_maps[i] = prepare_map.prepare_map(mod_maps[i], 512, freq, 100)
	#create the mask with the map at 857
	if (freq =="857"):
		the_mask = galaxy_mask.mask(mod_maps[i],6.3)
	i += 1


#Mask each map with the mask compute before
for j in range (0, len(freq_list)):
	mod_maps[j] = np.ma.masked_array(mod_maps[j], mask=the_mask)

W_mat_mask_512 =compute_sources_ILC_constrained.algo_ILC_constrained(mod_maps)[0]

mod_maps_half1 = np.ma.array([a,a,a,a,a,a])
mod_maps_half2 = np.ma.array([a,a,a,a,a,a])
                             
k=0
for freq in freq_list:
	mod_maps_half1[k] = hp.read_map("/home/abeelen/Planck/maps/HFI_SkyMap_" + freq + "_2048_R2.00_halfmission-1.full.fits")
	mod_maps_half1[k] = prepare_map.prepare_map(mod_maps_half1[i], 512, freq, 100)
        mod_maps_half2[k] = hp.read_map("/home/abeelen/Planck/maps/HFI_SkyMap_" + freq + "_2048_R2.00_halfmission-2.full.fits")
	mod_maps_half2[k] = prepare_map.prepare_map(mod_maps_half2[i], 512, freq, 100)
        mod_maps_half1[k] = np.ma.masked_array(mod_maps_half1[k], mask=the_mask)
        mod_maps_half2[k] = np.ma.masked_array(mod_maps_half2[k], mask=the_mask)

CMB_mask_half1_512=np.dot(np.ma.transpose(W_mat_mask_512)[0],mod_maps_half1)
CMB_mask_half2_512=np.dot(np.ma.transpose(W_mat_mask_512)[0],mod_maps_half2)

SZ_mask_half1_512=np.dot(np.ma.transpose(W_mat_mask_512)[1],mod_maps_half1)
SZ_mask_half2_512=np.dot(np.ma.transpose(W_mat_mask_512)[1],mod_maps_half2)

CMB_mask_diff_512=CMB_mask_half1_512-CMB_mask_half2_512
SZ_mask_diff_512=SZ_mask_half1_512-SZ_mask_half2_512




#ILC constrained algorithme adding a mask on the galaxy

import healpy as hp
import numpy as np
import prepare_map
import matplotlib.pyplot as plt
import compute_sources_ILC_constrained
import galaxy_mask


map_100 = hp.read_map("../data/HFI_SkyMap_100_512_homogenized_R2.00_full.fits")
map_143 = hp.read_map("../data/HFI_SkyMap_143_512_homogenized_R2.00_full.fits")
map_217 = hp.read_map("../data/HFI_SkyMap_217_512_homogenized_R2.00_full.fits")
map_353 = hp.read_map("../data/HFI_SkyMap_353_512_homogenized_R2.00_full.fits")
map_545 = hp.read_map("../data/HFI_SkyMap_545_512_homogenized_R2.00_full.fits")
map_857 = hp.read_map("../data/HFI_SkyMap_857_512_homogenized_R2.00_full.fits")

#create the mask with the map at 857
the_mask = galaxy_mask.mask(map_857,6.3)

#Mask each map with the mask compute before
map_100= np.ma.masked_array(map_100, mask=the_mask)
map_143= np.ma.masked_array(map_143, mask=the_mask)
map_217= np.ma.masked_array(map_217, mask=the_mask)
map_353= np.ma.masked_array(map_353, mask=the_mask)
map_545= np.ma.masked_array(map_545, mask=the_mask)
map_857= np.ma.masked_array(map_857, mask=the_mask)

mod_maps = np.ma.array([map_100,map_143,map_217,map_353,map_545,map_857])
W_mat, CMB, SZ =compute_sources_ILC_constrained.algo_ILC_constrained(mod_maps)


plt.ion()
hp.mollview(map_100,min=-5.E-5,max=5.e-5)
hp.mollview(CMB,min=-5.E-5,max=5e-5)
hp.mollview(SZ,min=-5.E-5,max=5e-5)

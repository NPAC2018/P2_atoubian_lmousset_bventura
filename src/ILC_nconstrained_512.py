#premier test with ILC algorithme

import healpy as hp
import numpy as np
import prepare_map
import matplotlib.pyplot as plt
import compute_sources_ILC
import matplotlib.pyplot as plt

freq_list = ["100","143","217","353","545","857"]

#Create a list containing all the maps
mod_maps = []
for freq in freq_list:
	mod_maps.append(hp.read_map("../data/HFI_SkyMap_" + freq +"_512_homogenized_R2.00_full.fits"))

CMB =compute_sources_ILC.algo_ILC(mod_maps)[0]
SZ = compute_sources_ILC.algo_ILC(mod_maps)[1]

plt.ion()

hp.mollview(CMB,min=-5.E-5,max=5.E-5)
hp.mollview(SZ,min=-5.E-5,max=5.E-5)

#compute the noise

import healpy as hp
import numpy as np
import prepare_map
import matplotlib.pyplot as plt
import compute_sources_ILC_constrained
import galaxy_mask
import make_spectrum

freq_list = ["100","143","217","353","545","857"]

#Create a list containing all the maps, prepare all of them
a=np.zeros(12*2048**2)

'''
i=0
for freq in freq_list:
        mod_maps[i] = hp.read_map("/home/abeelen/Planck/maps/HFI_SkyMap_" + freq + "_2048_R2.00_full.fits")
        mod_maps[i] = prepare_map.prepare_map(mod_maps[i], 2048, freq, 100)
        i+=1'''
        


W_mat_2048 = np.array( [[0.09495783330626553, -0.06481410901287692],
                        [0.3514310022212975, -0.13442341401313326],
                        [0.3446974159983963, 0.14493072771387774],
                        [0.2094063779101884, 0.05439976757893959],
                        [-0.0006021200171405564, -0.00010509341112108613],
                        [0.00010949058099290687, 1.2121144313945195e-05]])

#W_mat_2048 =np.array( [[-0.7350618398511424, 0.06718808136411482, 11280.969491168638],
#                       [1.0385660424339918, -0.24370194478312543, -9338.994816188362],
#                       [0.7306140325970308, 0.08355647317343434, -5245.072760052548],
#                       [-0.033857316542475806, 0.0930872126425155, 3306.247314585656],
#                       [-0.00034926211079264473, -0.00014530667180820075, -3.436644237915957],
#                       [8.834347338830118e-05, 1.5484274868839488e-05, 0.28741472452937106]])


mod_maps_half1 = np.array([a,a,a,a,a,a])
mod_maps_half2 = np.array([a,a,a,a,a,a])

                             
for k,freq2 in enumerate(freq_list):
        mod_maps_half1[k] = hp.read_map("/home/atoubian/HFI_SkyMap_"+freq2+"_2048_homogenized_masked_R2.00_half1.fits")
        mod_maps_half2[k] = hp.read_map("/home/atoubian/HFI_SkyMap_"+freq2+"_2048_homogenized_masked_R2.00_half2.fits")
        


mod_maps= np.array([a,a,a,a,a,a])


for i,freq in enumerate(freq_list):
        mod_maps[i] = hp.read_map("/home/abeelen/Planck/maps/HFI_SkyMap_" + freq + "_2048_R2.00_full.fits")
        mod_maps[i] = prepare_map.prepare_map(mod_maps[i], 2048, freq, 100)
        mod_maps[i][mod_maps_half1[i]==0]=0


        
CMB_half1_2048=np.dot(np.transpose(W_mat_2048)[0],mod_maps_half1)
CMB_half2_2048=np.dot(np.transpose(W_mat_2048)[0],mod_maps_half2)

SZ_half1_2048=np.dot(np.transpose(W_mat_2048)[1],mod_maps_half1)
SZ_half2_2048=np.dot(np.transpose(W_mat_2048)[1],mod_maps_half2)

CMB_diff_2048=(CMB_half1_2048-CMB_half2_2048)/2.
SZ_diff_2048=(SZ_half1_2048-SZ_half2_2048)/2.

#mask=galaxay_mask.mask(mod_maps[5])

#CMB_diff_512_mask=CMB_diff_512*(1-mask)
#SZ_diff_512_mask=SZ_diff_512*(1-mask)

Cl_nCMB = hp.sphtfunc.anafast(CMB_diff_2048,lmax=1700)
Cl_nSZ = hp.sphtfunc.anafast(SZ_diff_2048,lmax=1700)
l = np.arange(0, len(Cl_nCMB))

CMB_2048=np.dot(np.transpose(W_mat_2048)[0],mod_maps)
Cl_CMB = hp.sphtfunc.anafast(CMB_2048,lmax=1700)

SZ_2048=np.dot(np.transpose(W_mat_2048)[1],mod_maps)
Cl_SZ = hp.sphtfunc.anafast(SZ_2048,lmax=1700)

Gl=hp.sphtfunc.gauss_beam(0.00281,lmax=1700)

Cl_SZ_clean=(Cl_SZ-Cl_nSZ)/Gl**2
Dl_SZ_clean=l*(l+1)*Cl_SZ_clean

Cl_CMB_clean=(Cl_CMB-Cl_nCMB)/Gl**2
Dl_CMB_clean=l*(l+1)*Cl_CMB_clean
Dl_CMB=l*(l+1)*Cl_CMB


plt.figure()
plt.loglog(l,Cl_nCMB)
plt.title("Noise extracted from the CMB")
plt.xlabel("l")
plt.ylabel("C_l")
        
plt.figure()
plt.loglog(l,Cl_nSZ)
plt.title("Noise extracted from the SZ")
plt.xlabel("l")
plt.ylabel("C_l")

plt.figure()
plt.semilogx(l,Dl_CMB)
plt.title("CMB spectrum without noise treatment")
plt.xlabel("l")
plt.ylabel("D_l")

plt.figure()
plt.semilogx(l,Dl_CMB_clean)
plt.title("CMB spectrum after noise treatment")
plt.xlabel("l")
plt.ylabel("D_l")

plt.figure()
plt.loglog(l,Dl_SZ_clean)
plt.title("SZ spectrum after noise treatment")
plt.xlabel("l")
plt.ylabel("D_l")


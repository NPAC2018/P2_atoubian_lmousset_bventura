# create the filters for the function


import healpy as hp
import numpy as np
import filter_functions



#fgfg
def scaled_maps(mapf,n_side,l1,l2,l3):

    alms=hp.sphtfunc.map2alm(mapf,lmax=l3)

    
    limit=((l3+2)*(l3+1))//2
    print(limit)
    filter1=np.zeros(limit)
    filter2=np.zeros(limit)
    filter3=np.zeros(limit)

    limit1=(l1*(l1+1))//2
    limit2=(l2*(l2+1))//2
    filter1[0:limit1-1]=filter_functions.filter_func1(filter1[0:limit1-1])
    filter2[limit1:limit2-1]=filter_functions.filter_func2(filter2[limit1:limit2-1])
    filter3[limit2:]=filter_functions.filter_func3(filter3[limit2:])

    scale1=alms*filter1
    scale2=alms*filter2
    scale3=alms*filter3


    map1=hp.sphtfunc.alm2map(scale1,nside=n_side,lmax=l3)
    map2=hp.sphtfunc.alm2map(scale2,nside=n_side,lmax=l3)
    map3=hp.sphtfunc.alm2map(scale3,nside=n_side,lmax=l3)

    return map1, map2, map3

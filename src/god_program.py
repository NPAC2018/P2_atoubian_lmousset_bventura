import healpy as hp
import numpy as np
import prepare_map
import matplotlib.pyplot as plt
import compute_sources_ILC_constrained
import compute_sources_ILC_constrained_radio
import galaxy_mask
import filters

# Taking inputs
NSIDE= int(input("NSIDE = "))
mask= int(input("mask = "))
radio_correction= int(input("radio_correction = "))
multiscaling= int(input("multiscaling = "))

#List containing all frequences
freq_list = ["100","143","217","353","545","857"]

####Create a list containing all the maps, prepare all of them
a=np.ma.zeros(12*NSIDE**2)
mod_maps = np.ma.array([a,a,a,a,a,a], fill_value=0)

i=0
#Condition to open maps in 2048 or in 512
if (NSIDE == 2048):
	for freq in freq_list:
		mod_maps[i] = hp.read_map("/home/abeelen/Planck/maps/HFI_SkyMap_" + freq + "_2048_R2.00_full.fits")
		mod_maps[i] = prepare_map.prepare_map(mod_maps[i], 2048, freq, 100)
		i += 1
else:
	for freq in freq_list:
		mod_maps[i] = hp.read_map("../data/HFI_SkyMap_" + freq + "_512_homogenized_R2.00_full.fits")
		#no need to prepare them (already done)
		i +=1


####Condition to add a mask on the galaxy
i=0
if (mask):
	#Create the mask from map at frequence 857
	the_mask = galaxy_mask.mask(mod_maps[-1],15.)
		
	#Mask each map with the mask computed before
	for j in range (0, len(freq_list)):
		mod_maps[j] = np.ma.masked_array(mod_maps[j], mask=the_mask, fill_value=0)

####Condition to use multi-scale analysis or not
#If we don't want
if (not multiscaling):
	if(radio_correction):
		W_mat, CMB, SZ, RADIO = compute_sources_ILC_constrained_radio.algo_ILC_constrained_radio(mod_maps)
		if(mask):
			CMB.set_fill_value(np.nan)
			SZ.set_fill_value(np.nan)
			RADIO.set_fill_value(np.nan)
	else:
		W_mat, CMB, SZ = compute_sources_ILC_constrained.algo_ILC_constrained(mod_maps)
		if(mask):
			CMB.set_fill_value(np.nan)
			SZ.set_fill_value(np.nan)

#If we want multi-scale analysis
else:
	a=hp.sphtfunc.map2alm(mod_maps[0])
	mod_alm =np.full( (len(freq_list), len(a)), a)
	j=0
	for freq in freq_list:
		mod_alm[j] = hp.sphtfunc.map2alm(mod_maps[j])
		j+=1

	# mod_alm contains in each index the alms for each map

	size = len(mod_alm[0])
	l = np.array([0,50,1000,3*NSIDE-1])
	#l = np.array([0,3*NSIDE-1])
	gate= np.empty((len(l)-1,size))
	mod_alm_filtered =  np.empty((len(mod_alm), len(gate),size),dtype=complex)
	mod_maps_filtered = np.empty((len(mod_maps), len(gate),12*NSIDE**2 ))

	# i stands for the filter number i
	# j stands for the index of the map
	for i in range(0, len(gate)):
		gate[i]= filters.gate_function(l[i], l[i+1], size)

		for j in range(0,len(freq_list)):
			mod_alm_filtered[j,i,:] = hp.sphtfunc.almxfl(mod_alm[j], gate[i])

	# after all that  mod_alm_filtered contains in the first index the map, and in the second the different filters. need to apply alm2map

	for j in range(0,len(freq_list)):
		for i in range(0, len(gate)):
			mod_maps_filtered[j,i,:] = hp.sphtfunc.alm2map(mod_alm_filtered[j,i,:], NSIDE)
			if (mask):
				mod_maps_filtered[j,i,:] = np.ma.masked_array(mod_maps_filtered[j,i,:], mask=the_mask, fill_value=0)


	CMB = np.empty((len(gate), len(mod_maps[0])))
	SZ = np.empty((len(gate), len(mod_maps[0])))
	RADIO = np.empty((len(gate), len(mod_maps[0])))

	if(not radio_correction):
		for i in range(0, len(gate)):
			_ , CMB[i], SZ[i] = compute_sources_ILC_constrained.algo_ILC_constrained(mod_maps_filtered[:,i,:])
	else:
		for i in range(0, len(gate)):
			_, CMB[i], SZ[i], RADIO[i] = compute_sources_ILC_constrained_radio.algo_ILC_constrained_radio(mod_maps_filtered[:,i,:])

	plt.ion()

	CMB_tot = sum(CMB)
	SZ_tot = sum(SZ)
	RADIO_tot = sum(RADIO)

	hp.mollview(CMB_tot,min=-1.E-5,max=1e-5,title="CMB_"+str(NSIDE)+"_"+str(mask)+"_"+str(radio_correction)+"  multiscale = "+str(multiscaling))
	hp.mollview(SZ_tot,min=-1.E-5,max=1e-5,title="SZ_"+str(NSIDE)+"_"+str(mask)+"_"+str(radio_correction)+"  multiscale = "+str(multiscaling))


print("\nobject maps are called CMB, SZ, RADIO, W_mat.\nRecall : NSIDE = "+str(NSIDE)+"  mask = "+str(mask)+"  radio_correction = "+str(radio_correction)+"  multiscale = "+str(multiscaling))

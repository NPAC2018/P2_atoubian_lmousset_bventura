
#scaled ILC

import healpy as hp
import numpy as np
import prepare_map
import matplotlib.pyplot as plt
import compute_sources_ILC_constrained
import create_scaled_maps

#dfgsg
n_side=512
l1=50
l2=1000
l3=1500

freq_list = ["100","143","217","353","545","857"]

#Create a list containing all the maps

mod_maps1=[]
mod_maps2=[]
mod_maps3=[]

for freq in freq_list:
    mapf=hp.read_map("../data/HFI_SkyMap_" + freq +"_512_homogenized_R2.00_full.fits")
    maps=create_scaled_maps.scaled_maps(mapf,n_side,l1,l2,l3)
    mod_maps1.append(maps[0])
    mod_maps2.append(maps[1])
    mod_maps3.append(maps[2])

mod_maps=[mod_maps1,mod_maps2,mod_maps3]
        
'''for i in range(0,3):
    mod_mapsi=[]
    for freq in freq_list:
        mapf=hp.read_map("../data/HFI_SkyMap_" + freq +"_512_homogenized_R2.00_full.fits")
        mapi=create_scaled_maps.scaled_maps(mapf,n_side,l1,l2,l3)[i]
        mod_mapsi.append(mapi)
    mod_maps.append(mod_mapsi)'''
        
CMB=[]
SZ=[]

for k in range(0,3):
    CMB.append(compute_sources_ILC_constrained.algo_ILC_constrained(mod_maps[k])[1])
    SZ.append(compute_sources_ILC_constrained.algo_ILC_constrained(mod_maps[k])[2])

CMB_tot=CMB[0]+CMB[1]+CMB[2]
SZ_tot=SZ[0]+SZ[1]+SZ[2]

plt.ion()

hp.mollview(CMB_tot,min=-5.E-5,max=5.E-5)
hp.mollview(SZ_tot,min=-5.E-5,max=5.E-5)


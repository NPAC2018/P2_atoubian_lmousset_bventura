#Algorithm ILC constrained with radio emission

import healpy as hp
import numpy as np

def algo_ILC_constrained_radio(mod_maps):
    #this script takes an array of maps (already modified in order to be summed properly) as input and returns the contribution of each considered source

    #we first create the mixing matrix
    #the first colulmn corresponds to CMB, the second to SZ, the third to RADIO
    n_sources=3
    n_channels=6
    mix_mat=np.ma.zeros((n_channels,n_sources))
    for j in range(0,n_channels):
         mix_mat[j][0]=1

    #SZ column
    mix_mat[0][1]=-(1./0.24815)
    mix_mat[1][1]=-(1./0.35923)
    mix_mat[2][1]=(1./5.152)
    mix_mat[3][1]=(1./0.161098)
    mix_mat[4][1]=(1./0.06918)
    mix_mat[5][1]=(1./0.0380)

    #Radio correction column
    mix_mat[0][2]=100**(-0.7)/244.1
    mix_mat[1][2]=143**(-0.7)/371.74
    mix_mat[2][2]=217**(-0.7)/483.69
    mix_mat[3][2]=353**(-0.7)/287.45
    mix_mat[4][2]=545**(-0.7)/58.04
    mix_mat[5][2]=857**(-0.7)/2.27

    #compute the mean of the matrix
    mean = np.mean(mod_maps, axis=1)
    for i in range (0, len(mod_maps)):
        mod_maps[i] = mod_maps[i] - mean[i]

    #transpose the original matrix 
    transp_mod_maps=np.ma.transpose(mod_maps)

    #compute the number of pixels
    n_pixels=len(mod_maps[0])

    #compute the covariance matrix
    cov_mat=(1./n_pixels)*np.ma.dot(mod_maps,transp_mod_maps)

    #compute the inverse of the covariance matrix
    inv_cov_mat=np.linalg.inv(cov_mat)
   
    #create vectors to compute constrained sources
    e_CMB=np.ma.array([1,0,0])
    e_SZ=np.ma.array([0,1,0])
    e_RADIO=np.ma.array([0,0,1])
    
    #compute the value of each source term for each pixel 
    left_part = np.linalg.inv(np.dot(np.transpose(mix_mat),np.ma.dot(inv_cov_mat,mix_mat)))
    right_part = np.ma.dot(np.ma.transpose(mix_mat),np.ma.dot(inv_cov_mat,mod_maps))
    
    CMB = np.ma.dot(np.ma.dot(np.ma.transpose(e_CMB),left_part),right_part)
    SZ = np.ma.dot(np.dot(np.ma.transpose(e_SZ),left_part),right_part)
    RADIO = np.ma.dot(np.ma.dot(np.ma.transpose(e_RADIO),left_part),right_part)

    # Compute weights of the lagrange multipliers
    W_mat = np.ma.dot(np.ma.dot(inv_cov_mat, mix_mat), left_part)
    
    return W_mat, CMB, SZ, RADIO

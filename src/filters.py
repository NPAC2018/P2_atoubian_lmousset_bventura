
import numpy as np


def gate_function(l1,l2,size):
	gate = np.zeros(size)
	gate[l1:l2]=1
	return gate

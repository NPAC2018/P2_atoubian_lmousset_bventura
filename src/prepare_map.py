
import numpy as np
import healpy as hp

# This function uses the data collected in table 6 of Planck 2013 results. VII. HFI time response and beams
# https://arxiv.org/pdf/1303.5068.pdf
def convert_freq_fwhm(freq):
    return {
	"100": 0.00281,
        "143": 0.00211,
	"217": 0.00145,
	"353": 0.00140,
	"545": 0.00136,
	"857": 0.00126
     }.get(freq, 0.00281)


# This function homogenize the resolution of the input map & frequency with the worst frequency given 
def homogenize_freq(map,freq, worst_freq):
	worst_fwhm= convert_freq_fwhm(worst_freq)
	fwhm= convert_freq_fwhm(freq)
	return hp.sphtfunc.smoothing(map,fwhm=np.sqrt(worst_fwhm**2 - fwhm**2))
            

# Function to reduce all maps to the same units
# 545 & 857 maps are given in other units than the others
def homogenize_units(map,freq):
	conversion_factor={
	        "545": 58.04,
	        "857": 2.27
	        }.get(freq, 1.)
	return map/conversion_factor
    # The dictionnary checks over freq, and by default give map for others

# This function prepares the map by ud_grading it to nside, homogenize the units for two specific maps and homogenize the resolution of the maps given a worst frequency
def prepare_map(map, nside, freq, worst_freq, write=0, name=''):
    map = hp.ud_grade(map, nside_out=nside)
    map = homogenize_units(map, freq)
    map = homogenize_freq(map, freq, worst_freq)
    if write:
        hp.fitsfunc.write_map(name, map)
    return map

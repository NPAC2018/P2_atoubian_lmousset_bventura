#Algorithm ILC

import healpy as hp
import numpy as np

def algo_ILC(mod_maps):
    #this script takes an array of maps (already modified in order to be summed properly) as input and returns the contribution of each considered source

    #we first create the mixing matrix
    #the first colulmn corresponds to CMB and the second to SZ
    n_sources=2
    n_channels=6
    mix_mat=np.ma.zeros((n_channels,n_sources))
    for j in range(0,n_channels):
         mix_mat[j][0]=1

    mix_mat[0][1]=-(1./0.24815)
    mix_mat[1][1]=-(1./0.35923)
    mix_mat[2][1]=(1./5.152)
    mix_mat[3][1]=(1./0.161098)
    mix_mat[4][1]=(1./0.06918)
    mix_mat[5][1]=(1./0.0380)

    #compute the mean of the matrix
    mean = np.mean(mod_maps, axis=1)
    for i in range (0, len(mod_maps)):
        mod_maps[i] = mod_maps[i] - mean[i]

    #transpose the original matrix 
    transp_mod_maps=np.ma.transpose(mod_maps)

    #compute the number of pixels
    n_pixels=len(mod_maps[0])

    #compute the covariance matrix
    cov_mat=(1./n_pixels)*np.ma.dot(mod_maps,transp_mod_maps)

    #compute the inverse of the covariance matrix
    inv_cov_mat=np.linalg.inv(cov_mat)

    #compute the value of each sourc term for each pixel 
    for i in range(0,n_sources):
         sp_em=np.ma.transpose(mix_mat)[i]
         down_part=  np.ma.dot(np.transpose(sp_em),np.ma.dot(inv_cov_mat,sp_em))
         up_part=np.ma.dot(np.ma.transpose(sp_em),np.ma.dot(inv_cov_mat,mod_maps))
         sources[i]=up_part/down_part

    
    #the first line corresponds to CMB and the second to SZ
    return sources
        
    

    

    

    

